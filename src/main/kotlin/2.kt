fun aDoubleoU(num : Any): Double{
    try{
        return num.toString().toDouble()
    }catch (e: NumberFormatException){
        return 1.0
    }
}

fun main(){
    println(aDoubleoU(7.1))
    println(aDoubleoU(9))
    println(aDoubleoU(.2))
    println(aDoubleoU("tres"))
}