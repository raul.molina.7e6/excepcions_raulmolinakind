import java.io.File
import java.io.IOException
import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada = scanner.next()
    try{
        val texto = File(entrada).readText()
        println(texto)
    }catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida")
    }
}