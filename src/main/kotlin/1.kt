fun divideixoCero(dividend: Int, divisor: Int){
    try{
        println(dividend/divisor)
    }catch(e: ArithmeticException){
        println(0)
    }
}
fun main(){
    divideixoCero(7, 2)
    divideixoCero(8, 4)
    divideixoCero(5, 0)
}